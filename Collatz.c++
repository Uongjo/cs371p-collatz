// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Collatz.h"

using namespace std;

int lazyCache[10000000];

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

int cycle_length (int n) {
    assert(n > 0);
    // Using Lazy Cache --> Check if value is in the cache
    if(lazyCache[n] != 0) {
        return lazyCache[n];
    }
    // Int overflow was an issue --> long long is 64 bits
    long long num = n;
    int len = 1;
    while (num > 1) {
        if ((num % 2) == 0) {
            num = (num / 2);
            ++len;
        } else {
            // We already know that 3n + 1 returns even, so we divide by 2
            num = (3 * num + 1)/2;
            // Increment twice to accomodate change
            ++++len;
        }
    }
    // Update Cache
    lazyCache[n] = len;
    assert(len > 0);
    return len;
}

int max_cycle_length(int start, int end) {
    int maxLen = -1;
    for(int i = start; i <= end; ++i) {
        maxLen = max(maxLen, cycle_length(i));
    }
    assert(maxLen > -1);
    return maxLen;
}

int collatz_eval (int i, int j) {
    int start = min(i, j);
    int end = max(i, j);
    int maxLen = -1;

    assert(start < 1000000);
    assert(end < 1000000);
    assert(start > 0);
    assert(end > 0);

    // Reducing the interval to which we have to search
    int newStart = (end/2) + 1;
    if(start < newStart) {
        start = newStart;
    }

    maxLen = max(maxLen, max_cycle_length(start, end));
    assert(maxLen > -1);
    return maxLen;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
