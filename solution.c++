#include <cassert>  // assert
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int lazyCache[10000000];

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);}

int cycle_length (int n) {
    if(lazyCache[n] != 0){
        return lazyCache[n];
    }
    long long num = n;
    int len = 1;
    while (num > 1) {
        if ((num % 2) == 0){
            num = (num / 2);
            ++len;
        }else{
          num = (3 * num + 1)/2;
          ++++len;
        }
    }
    lazyCache[n] = len;
    return len;
}

int max_cycle_length(int start, int end){
    int maxLen = -1;
    for(int i = start; i <= end; ++i){
        maxLen = max(maxLen, cycle_length(i));
    }
    return maxLen;
}

int collatz_eval (int i, int j) {
    int start = min(i, j);
    int end = max(i, j);
    int maxLen = -1;
    int newStart = (end/2) + 1;
    if(start < newStart){
        start = newStart;
    }
    maxLen = max(maxLen, max_cycle_length(start, end));
    return maxLen;
}

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);}
}

int main () {
    using namespace std;
    collatz_solve(cin, cout);
    return 0;
}
